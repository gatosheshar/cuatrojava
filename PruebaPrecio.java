/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package precio;

/**
 *
 * @author Administrador
 */
public class PruebaPrecio {
    public static void main (String [] args ) {
        Precio p; // Crea una referencia de la clase Precio}
        p =new Precio(); // Crea el objeto de la clase Precio
        p.pone(56.8);
        // Llamada al metodo pone
        // que asigna 56.8 al atributo euros
        System.out.println("Valor = " +p.da()); // Llamada al metodo da
        // que devuelve el valor de euros
        Precio q =new Precio(); // Crea una referencia y el objeto
        q.euros=75.6; // Asigna 75.6 al atributo euros
        System.out.println("Valor = " +q.euros);
    }
}
